import http.client, urllib.parse, urllib.error, base64
import json, datetime
from flask import make_response

headers = {
    'Content-Type': 'application/json',
}

params = urllib.parse.urlencode({
    'Organization': 'd70703ec-efc3-47cf-a5f3-d80f7c07e718',
    'apitoken':'CC13DBA75DE249D7847266CF05C06808'
})

conn = http.client.HTTPSConnection('api.nibo.com.br')

id_bradesco_account = "627a8e8f-5cd0-4fb3-ba85-96f35c7c9f22"
id_despesa_categories = "0bf3b960-86f2-4023-a295-47da4e164e51"

#CRIAÇÃO DE CLIENTE
def handler(request):

    if 'data' in request:
        req = base64.b64decode(request['data']).decode('utf-8')
        payload = json.loads(req)

        try:
            plate ='{}-{}'.format(payload['plate'][:3], payload['plate'][3:7])
            json_string_client = {"name": plate}
            data = json.dumps(json_string_client)
            conn.request("POST", f"/empresas/v1/customers?{params}", data, headers)
            responseClient = conn.getresponse()

            #CRIAÇÃO DE CUSTO"
            if responseClient.code == 200:
                id_client = responseClient.read().decode().replace("\"", "")
                json_string_cost = {
                    "description": plate,
                    "externalCode": str(payload['orderNumber'])}
                data = json.dumps(json_string_cost)
                print(data)
                conn.request("POST", f"/empresas/v1/costcenters?{params}", data, headers)
                response = conn.getresponse()

                if response.code == 200:
                    id_cost = response.read().decode().replace("\"", "")
                    sendReceipts(id_client, id_cost, payload['payments'])
                    conn.close()

                else:
                    return make_response("Erro created", 500)

            else:
                return make_response("Erro created", 500)

        except Exception as e:
            return make_response(str(e), 500)


#CRIAÇÃO DE RECEBIMENTO"
def sendReceipts(id_client,id_cost, payments):

    try:
        for p in payments:
            json_string_receipts = {
                "accountId": id_bradesco_account,
                "stakeholderId": id_client,
                "categoryId": id_despesa_categories,
                "costCenterId": id_cost,
                "value": p['value'],
                "date": datetime.datetime.now().isoformat(),
                "description": p['paymentType']['title'],
                "isFlag": "true"}

            print(json_string_receipts)
            data = json.dumps(json_string_receipts)
            print(data)
            conn.request("POST", f"/empresas/v1/receipts/FormatType=json?{params}", data, headers)
            response = conn.getresponse()
            data = response.read()
            print(data)
            conn.close()

    except Exception as e:
        return make_response(str(e), 500)
