from main import handler

if __name__ == '__main__':
    import json
    import base64
    handler({"data": base64.b64encode(json.dumps(
        {
            "id" : "5d02582b4261eb2a3e23b080",
            "inspectionKey" : 6035509994323968,
            "buyer" : {
                "name" : "YURI",
                "email" : "YURI@VOLANTY.COM",
                "document" : "05241636773",
                "address" : "RUA DO COUTO",
                "addressNumber" : "100",
                "addressComplement" : "",
                "state" : "RJ",
                "cellphoneNumber" : "(23) 1312-31231",
                "city" : "RIO DE JANEIRO",
                "neighborhood" : "PENHA",
                "zipCode" : 21020410,
                "receipts" : {
                    "cnh" : "https://s3.sa-east-1.amazonaws.com/com.volanty.sec.hml/order/6035509994323968/20190613/document_1560423925921.jpg"
                }
            },
            "cav" : {
                "key" : 6561742637236224,
                "name" : "BARRA DA TIJUCA",
                "description" : "Barra da Tijuca"
            },
            "payments" : [ {
                "paymentType" : {
                    "id" : "stone",
                    "title" : "Stone",
                    "description" : "Pagamento via POS Stone"
                },
                "value" : 500.0,
                "obs" : "SINAL",
                "paymentStatus" : "PENDING"
            } ],
            "additional" : [ ],
            "reserveDate" : "2019-06-13T14:05:25.250+0000",
            "volantySalesCommission" : 1120.0,
            "salePriceValue" : 16000.0,
            "status" : "RESERVED",
            "userUpdate" : {
                "email" : "yuri@volanty.com",
                "date" : "2019-06-13T14:05:30.946+0000",
                "name" : "Yuri Pacheco"
            },
            "orderNumber" : 164,
            "responsible" : "yuri@volanty.com",
            "plate" : "EUN2238"
        }
    ).encode('utf-8'))})